CREATE EXTENSION "uuid-ossp";

CREATE TABLE "payments" (
  "id" UUID PRIMARY KEY DEFAULT uuid_generate_v1mc(),
  "type" varchar NOT NULL,
  "version" int DEFAULT 0,
  "organisation_id" varchar,
  "attributes" jsonb NOT NULL default '{}'::jsonb,
  "created_at" timestamp without time zone DEFAULT NOW()::timestamp
);