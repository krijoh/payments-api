-- this file should be encrypted with for example sops in production, user is not used in k8s deployment
CREATE ROLE paymentadmin WITH LOGIN NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION ENCRYPTED PASSWORD 'pass';
