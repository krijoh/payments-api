package payment

import (
	"payments-api/pkg/errs"

	"github.com/google/uuid"
	"github.com/pkg/errors"
)

type Service struct {
	repo Repository
}

func New(r Repository) *Service {
	return &Service{r}
}

// FindByID finds a Payment using id.
func (s *Service) FindByID(id string) (*Payment, error) {
	if _, err := uuid.Parse(id); err != nil {
		return nil, errs.ValidationError{errors.New("invalid ID must be a UUID")}
	}

	return s.repo.FindPaymentByID(id)
}

// FindAll Payments using pagination.
func (s *Service) FindAll(page, limit string) ([]*Payment, error) {
	return s.repo.FindAllPayments(page, limit)
}

// Create a new Payment.
func (s *Service) Create(p *Payment) error {
	if err := p.Validate(); err != nil {
		return errs.ValidationError{err}
	}

	return s.repo.CreatePayment(p)
}

// Delete a Payment.
func (s *Service) DeleteByID(id string) error {
	if _, err := uuid.Parse(id); err != nil {
		return errs.ValidationError{errors.New("invalid ID must be a UUID")}
	}

	return s.repo.DeletePaymentByID(id)
}

// Update new Payment.
func (s *Service) Update(p *Payment) error {
	if _, err := uuid.Parse(p.ID); err != nil {
		return errs.ValidationError{errors.New("invalid ID must be a UUID")}
	}

	if err := p.Validate(); err != nil {
		return errs.ValidationError{err}
	}

	return s.repo.UpdatePayment(p)
}
