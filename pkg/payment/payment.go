package payment

import (
	"time"

	validator "gopkg.in/go-playground/validator.v9"
)

type Payment struct {
	Type           string      `json:"type" validate:"required"`
	ID             string      `json:"id" sql:",pk"`
	Version        int         `json:"version"`
	OrganisationID string      `json:"organisation_id" sql:",nopk" validate:"required"`
	Attributes     *Attributes `json:"attributes" sql:"type:jsonb" validate:"required,dive"`
	CreatedAt      time.Time   `json:"-"`
}

// Validate a Payment.
func (p *Payment) Validate() error {
	validate := validator.New()
	return validate.Struct(p)
}

type Attributes struct {
	Amount               string              `json:"amount" validate:"required"`
	BeneficiaryParty     *Account            `json:"beneficiary_party" validate:"required,dive"`
	ChargesInformation   *ChargesInformation `json:"charges_information" validate:"required,dive"`
	Currency             string              `json:"currency" validate:"required"`
	DebtorParty          *Account            `json:"debtor_party" validate:"required"`
	EndToEndReference    string              `json:"end_to_end_reference" validate:"required"`
	Fx                   *Fx                 `json:"fx" validate:"required,dive"`
	NumericReference     string              `json:"numeric_reference" validate:"required"`
	PaymentID            string              `json:"payment_id" validate:"required"`
	PaymentPurpose       string              `json:"payment_purpose" validate:"required"`
	PaymentScheme        string              `json:"payment_scheme" validate:"required"`
	PaymentType          string              `json:"payment_type" validate:"required"`
	ProcessingDate       string              `json:"processing_date" validate:"required"`
	Reference            string              `json:"reference" validate:"required"`
	SchemePaymentSubType string              `json:"scheme_payment_sub_type" validate:"required"`
	SchemePaymentType    string              `json:"scheme_payment_type" validate:"required"`
	SponsorParty         *Account            `json:"sponsor_party" validate:"required,dive"`
}

type Account struct {
	AccountName       string `json:"account_name,omitempty"`
	AccountNumber     string `json:"account_number" validate:"required"`
	AccountNumberCode string `json:"account_number_code,omitempty"`
	AccountType       int    `json:"account_type,omitempty"`
	Address           string `json:"address,omitempty"`
	BankID            string `json:"bank_id" validate:"required"`
	BankIDCode        string `json:"bank_id_code" validate:"required"`
	Name              string `json:"name,omitempty"`
}

type ChargesInformation struct {
	BearerCode              string    `json:"bearer_code" validate:"required"`
	ReceiverChargesAmount   string    `json:"receiver_charges_amount" validate:"required"`
	ReceiverChargesCurrency string    `json:"receiver_charges_currency" validate:"required"`
	SenderCharges           []*Charge `json:"sender_charges" validate:"required,dive"`
}

type Charge struct {
	Amount   string `json:"amount" validate:"required"`
	Currency string `json:"currency" validate:"required"`
}

type Fx struct {
	ContractReference string `json:"contract_reference" validate:"required"`
	ExchangeRate      string `json:"exchange_rate" validate:"required"`
	OriginalAmount    string `json:"original_amount" validate:"required"`
	OriginalCurrency  string `json:"original_currency" validate:"required"`
}
