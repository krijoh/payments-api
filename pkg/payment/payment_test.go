package payment

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestPayment_Validate(t *testing.T) {
	type fields struct {
		payment *Payment
	}

	tests := []struct {
		name    string
		fields  fields
		want    []byte
		wantErr bool
	}{
		{
			"Validate s Payment",
			fields{
				payment: &Payment{
					ID:             "4ee3a8d8-ca7b-4290-a52c-dd5b6165ec43",
					Type:           "Payment",
					Version:        0,
					OrganisationID: "743d5b63-8e6f-432e-a8fa-c5d8d2ee5fcb", // TODO: add validation for this
					Attributes: &Attributes{
						Amount: "200",
						BeneficiaryParty: &Account{
							AccountName:       "beneficiary",
							AccountNumber:     "123",
							AccountNumberCode: "1",
							AccountType:       1,
							Address:           "ben street 1",
							BankID:            "001",
							BankIDCode:        "bic123",
							Name:              "ben name",
						},
						ChargesInformation: &ChargesInformation{
							BearerCode:              "bearer code",
							SenderCharges:           []*Charge{&Charge{Amount: "100", Currency: "USD"}},
							ReceiverChargesAmount:   "100",
							ReceiverChargesCurrency: "USD",
						},
						Currency: "USD",
						DebtorParty: &Account{
							AccountName:       "debtor",
							AccountNumber:     "123",
							AccountNumberCode: "1",
							Address:           "debtor street 1",
							Name:              "the debtor",
							BankID:            "002",
							BankIDCode:        "bic124",
						},
						EndToEndReference: "endtoend",
						Fx: &Fx{
							ContractReference: "contract ref",
							ExchangeRate:      "1",
							OriginalAmount:    "100",
							OriginalCurrency:  "USD",
						},
						NumericReference:     "12345",
						PaymentID:            "1234",
						PaymentPurpose:       "purpose",
						PaymentScheme:        "scheme",
						PaymentType:          "Credit",
						ProcessingDate:       "20190525",
						Reference:            "ref",
						SchemePaymentSubType: "scheme sub",
						SchemePaymentType:    "scheme payment type",
						SponsorParty: &Account{
							AccountNumber: "123",
							BankID:        "003",
							BankIDCode:    "bic234",
						},
					},
				},
			},
			readTestFile("testdata/payment.json", t),
			false,
		},
		{
			"Validate a Payment with missing fields, should fail",
			fields{
				payment: &Payment{
					Version:        0,
					OrganisationID: "1",
				},
			},
			nil,
			true,
		},
	}

	requires := require.New(t)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			err := tt.fields.payment.Validate()
			if tt.wantErr {
				requires.Error(err)
				return
			}
			requires.NoError(err)

			var p Payment
			err = json.Unmarshal(tt.want, &p)
			requires.NoError(err)
			requires.Equal(tt.fields.payment, &p)
		})
	}
}

func readTestFile(file string, t *testing.T) []byte {
	dir, err := os.Getwd()
	if err != nil {
		t.Fatalf("unable to get working directory: %v", err)
	}

	b, err := ioutil.ReadFile(filepath.Join(dir, file))
	if err != nil {
		t.Fatalf("unable to read file: %v", err)
	}

	return b
}
