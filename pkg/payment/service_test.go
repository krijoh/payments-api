package payment

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestNew(t *testing.T) {
	type args struct {
		r Repository
	}
	tests := []struct {
		name string
		args args
	}{
		{
			"Initialize a new Service",
			args{&MockRepository{}},
		},
	}

	requires := require.New(t)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := New(tt.args.r)
			requires.NotNil(got)
		})
	}
}

func TestService_FindByID(t *testing.T) {
	type args struct {
		id string
	}

	tests := []struct {
		name    string
		args    args
		want    *Payment
		wantErr error
	}{
		{
			"Find Payment By ID",
			args{"15eb6731-5861-42f4-9634-57175ed460c8"},
			&Payment{ID: "15eb6731-5861-42f4-9634-57175ed460c8"},
			nil,
		},
	}

	requires := require.New(t)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MockRepository{}
			s := &Service{repo: m}

			m.On("FindPaymentByID", tt.args.id).Return(tt.want, tt.wantErr)

			got, err := s.FindByID(tt.args.id)
			if tt.wantErr != nil {
				requires.Error(err)
				return
			}

			requires.NoError(err)
			requires.Equal(tt.want, got)
			m.AssertExpectations(t)
		})
	}
}

func TestService_FindAll(t *testing.T) {
	type args struct {
		page  string
		limit string
	}

	tests := []struct {
		name    string
		args    args
		want    []*Payment
		wantErr error
	}{
		{
			"Find all Payments",
			args{"1", "10"},
			[]*Payment{&Payment{ID: "1"}},
			nil,
		},
	}

	requires := require.New(t)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MockRepository{}
			s := &Service{repo: m}

			m.On("FindAllPayments", tt.args.page, tt.args.limit).Return(tt.want, tt.wantErr)

			got, err := s.FindAll(tt.args.page, tt.args.limit)
			if tt.wantErr != nil {
				requires.Error(err)
				return
			}

			requires.NoError(err)
			requires.Equal(tt.want, got)
			m.AssertExpectations(t)
		})
	}
}

func TestService_Create(t *testing.T) {
	type args struct {
		p *Payment
	}

	tests := []struct {
		name    string
		args    args
		wantErr error
	}{
		{
			"Create a Payment",
			args{
				&Payment{
					ID:             "15eb6731-5861-42f4-9634-57175ed460c8",
					Type:           "Payment",
					Version:        0,
					OrganisationID: "1",
					Attributes: &Attributes{
						Amount: "200",
						BeneficiaryParty: &Account{
							AccountName:       "beneficiary",
							AccountNumber:     "123",
							AccountNumberCode: "1",
							AccountType:       1,
							Address:           "ben street 1",
							Name:              "the beneficiary",
							BankID:            "bank id",
							BankIDCode:        "bic123",
						},
						ChargesInformation: &ChargesInformation{
							BearerCode:              "bearer code",
							SenderCharges:           []*Charge{&Charge{Amount: "100", Currency: "USD"}},
							ReceiverChargesAmount:   "100",
							ReceiverChargesCurrency: "USD",
						},
						Currency: "USD",
						DebtorParty: &Account{
							AccountName:       "beneficiary",
							AccountNumber:     "123",
							AccountNumberCode: "1",
							Address:           "ben street 1",
							Name:              "the beneficiary",
							BankID:            "bank id",
							BankIDCode:        "bic123",
						},
						EndToEndReference: "endtoend",
						Fx: &Fx{
							ContractReference: "contract ref",
							ExchangeRate:      "1",
							OriginalAmount:    "100",
							OriginalCurrency:  "USD",
						},
						NumericReference:     "12345",
						PaymentID:            "123-uuid",
						PaymentPurpose:       "purpose",
						PaymentScheme:        "scheme",
						PaymentType:          "Payment",
						ProcessingDate:       "20190525",
						Reference:            "ref",
						SchemePaymentSubType: "scheme sub",
						SchemePaymentType:    "scheme payment type",
						SponsorParty: &Account{
							AccountNumber: "123",
							BankID:        "bank id",
							BankIDCode:    "bic123",
						},
					},
				},
			},
			nil,
		},
	}

	requires := require.New(t)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MockRepository{}
			s := &Service{repo: m}

			m.On("CreatePayment", tt.args.p).Return(tt.wantErr)

			err := s.Create(tt.args.p)
			if tt.wantErr != nil {
				requires.Error(err)
				return
			}

			requires.NoError(err)
			m.AssertExpectations(t)
		})
	}
}

func TestService_Delete(t *testing.T) {
	type args struct {
		id string
	}

	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			"Delete a Payment",
			args{"15eb6731-5861-42f4-9634-57175ed460c8"},
			false,
		},
		{
			"Delete a Payment with invalid UUID",
			args{"15eb6731-5861-42f4-9634-57175ed"},
			true,
		},
	}

	requires := require.New(t)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MockRepository{}
			s := &Service{repo: m}

			m.On("DeletePaymentByID", tt.args.id).Return(nil)

			err := s.DeleteByID(tt.args.id)
			if tt.wantErr {
				requires.Error(err)
				return
			}

			requires.NoError(err)
			m.AssertExpectations(t)
		})
	}
}

func TestService_Update(t *testing.T) {
	type args struct {
		p *Payment
	}

	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			"Update a Payment",
			args{
				&Payment{
					ID:             "15eb6731-5861-42f4-9634-57175ed460c8",
					Type:           "Payment",
					Version:        0,
					OrganisationID: "1",
					Attributes: &Attributes{
						Amount: "200",
						BeneficiaryParty: &Account{
							AccountName:       "beneficiary",
							AccountNumber:     "123",
							AccountNumberCode: "1",
							AccountType:       1,
							Address:           "ben street 1",
							Name:              "the beneficiary",
							BankID:            "bank id",
							BankIDCode:        "bic123",
						},
						ChargesInformation: &ChargesInformation{
							BearerCode:              "bearer code",
							SenderCharges:           []*Charge{&Charge{Amount: "100", Currency: "USD"}},
							ReceiverChargesAmount:   "100",
							ReceiverChargesCurrency: "USD",
						},
						Currency: "USD",
						DebtorParty: &Account{
							AccountName:       "beneficiary",
							AccountNumber:     "123",
							AccountNumberCode: "1",
							Address:           "ben street 1",
							Name:              "the beneficiary",
							BankID:            "bank id",
							BankIDCode:        "bic123",
						},
						EndToEndReference: "endtoend",
						Fx: &Fx{
							ContractReference: "contract ref",
							ExchangeRate:      "1",
							OriginalAmount:    "100",
							OriginalCurrency:  "USD",
						},
						NumericReference:     "12345",
						PaymentID:            "123-uuid",
						PaymentPurpose:       "purpose",
						PaymentScheme:        "scheme",
						PaymentType:          "Payment",
						ProcessingDate:       "20190525",
						Reference:            "ref",
						SchemePaymentSubType: "scheme sub",
						SchemePaymentType:    "scheme payment type",
						SponsorParty: &Account{
							AccountNumber: "123",
							BankID:        "bank id",
							BankIDCode:    "bic123",
						},
					},
				},
			},
			false,
		},
		{
			"Update a Payment with missing fields, should fail",
			args{
				&Payment{
					ID:   "15eb6731-5861-42f4-9634-57175ed460c8",
					Type: "Payment",
				},
			},
			true,
		},
	}

	requires := require.New(t)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MockRepository{}
			s := &Service{repo: m}

			m.On("UpdatePayment", tt.args.p).Return(nil)

			err := s.Update(tt.args.p)
			if tt.wantErr {
				requires.Error(err)
				return
			}

			requires.NoError(err)
			m.AssertExpectations(t)
		})
	}
}
