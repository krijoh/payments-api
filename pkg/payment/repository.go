package payment

type Repository interface {
	FindPaymentByID(id string) (*Payment, error)
	FindAllPayments(page, limit string) ([]*Payment, error)
	CreatePayment(p *Payment) error
	DeletePaymentByID(id string) error
	UpdatePayment(p *Payment) error
}
