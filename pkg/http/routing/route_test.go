package routing

import (
	"errors"
	http "net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

// TODO: remove
func TestRoute_ServeHTTP(t *testing.T) {
	tests := []struct {
		name        string
		r           Route
		req         *http.Request
		wantCode    int
		wantHeaders []string
		wantBody    string
	}{
		{
			"Set content type, write headers and response",
			Route(func(r *http.Request) *Response {
				return JSON(
					http.StatusOK,
					struct {
						Message string `json:"message"`
					}{
						Message: "value",
					},
					Headers{},
				)
			}),
			httptest.NewRequest("", "http://localhost", nil),
			http.StatusOK,
			[]string{"Content-Type", "application/json"},
			`{"data":{"message":"value"}}`,
		},
	}

	asserts := assert.New(t)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			recorder := httptest.NewRecorder()
			tt.r.ServeHTTP(recorder, tt.req)

			wantHeaders := http.Header{}
			wantHeaders.Add(tt.wantHeaders[0], tt.wantHeaders[1])

			asserts.Equal(tt.wantCode, recorder.Code)
			asserts.Equal(wantHeaders, recorder.Header())
			asserts.Equal(tt.wantBody, recorder.Body.String())
		})
	}
}

func TestHandleRoute(t *testing.T) {
	type args struct {
		ro Route
	}
	tests := []struct {
		name        string
		args        args
		req         *http.Request
		wantCode    int
		wantHeaders http.Header
		wantBody    string
	}{
		{
			"Set content-type and write StatusOK, headers and response",
			args{Route(func(r *http.Request) *Response {
				return JSON(
					http.StatusOK,
					struct {
						Message string `json:"message"`
					}{
						Message: "value",
					},
					Headers{"key": "value"},
				)
			}),
			},
			httptest.NewRequest("", "http://localhost", nil),
			http.StatusOK,
			http.Header{
				"Content-Type": []string{"application/json"},
				"Key":          []string{"value"},
			},
			`{"data":{"message":"value"}}`,
		},
		{
			"Set content-type and  write StatusInternalServerError, default headers and error response",
			args{Route(func(r *http.Request) *Response {
				return ErrorJSON(http.StatusOK, errors.New("an error"), Headers{})
			})},
			httptest.NewRequest("", "http://localhost", nil),
			http.StatusOK,
			http.Header{"Content-Type": []string{"application/json"}},
			`{"error":"an error"}`,
		},
	}

	asserts := assert.New(t)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			recorder := httptest.NewRecorder()

			handlerFunc := HandlerFunc(tt.args.ro)
			handlerFunc.ServeHTTP(recorder, tt.req)

			asserts.Equal(tt.wantCode, recorder.Code)
			asserts.Equal(tt.wantHeaders, recorder.HeaderMap)
			asserts.Equal(tt.wantBody, recorder.Body.String())

		})
	}
}
