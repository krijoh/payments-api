package routing

import (
	"net/http"
)

// Route is used to wrap handlers which returns a Response.
type Route func(r *http.Request) *Response

// ServeHTTP handles the request and returns the response for a Route.
func (r Route) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	if resp := r(req); resp != nil {
		if resp.ContentType != "" {
			rw.Header().Set("Content-Type", resp.ContentType)
		}

		for k, v := range resp.Headers {
			rw.Header().Set(k, v)
		}

		rw.WriteHeader(resp.StatusCode)
		rw.Write(resp.Content)
		return
	}

	rw.WriteHeader(http.StatusOK)
}

// HandlerFunc wraps a route in a http.HandlerFunc and serves the request.
func HandlerFunc(ro Route) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ro.ServeHTTP(w, r)
	})
}
