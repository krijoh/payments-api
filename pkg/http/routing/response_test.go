package routing

import (
	"errors"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestError(t *testing.T) {
	type args struct {
		statusCode int
		err        error
		headers    Headers
	}

	tests := []struct {
		name string
		args args
		want *Response
	}{
		{
			"An error Response",
			args{
				statusCode: http.StatusInternalServerError,
				err:        errors.New("an error occured"),
				headers:    Headers{"key": "value"},
			},
			&Response{
				StatusCode: http.StatusInternalServerError,
				Headers:    Headers{"key": "value"},
				Content:    []byte(errors.New("an error occured").Error()),
			},
		},
	}

	requires := require.New(t)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := ErrorResponse(tt.args.statusCode, tt.args.err, tt.args.headers)
			requires.Equal(tt.want, got)
		})
	}
}

func TestJSON(t *testing.T) {
	type args struct {
		statusCode int
		v          interface{}
		headers    Headers
	}

	tests := []struct {
		name string
		args args
		want *Response
	}{
		{
			"Response with JSON",
			args{
				statusCode: http.StatusOK,
				v: struct {Message string `json:"message"`}{Message: "a message"},
				headers: Headers{},
			},
			&Response{
				StatusCode: http.StatusOK,
				Content:    []byte(`{"data":{"message":"a message"}}`),
			},
		},
		{
			"Response with error JSON",
			args{
				statusCode: http.StatusOK,
				v:          make(chan int),
				headers:    Headers{},
			},
			&Response{
				StatusCode: http.StatusInternalServerError,
				Content:    []byte(`{"error":"json: unsupported type: chan int"}`),
			},
		},
	}

	asserts := assert.New(t)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := JSON(tt.args.statusCode, tt.args.v, tt.args.headers)
			asserts.Equal(tt.want.StatusCode, got.StatusCode)
			asserts.Equal(string(tt.want.Content), string(got.Content))
		})
	}
}
