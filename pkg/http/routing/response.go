package routing

import (
	"encoding/json"
	"net/http"
)

const (
	contentTypeApplicationJSON = "application/json"
)

// Headers for a response.
type Headers map[string]string

// Response for a http request.
type Response struct {
	StatusCode  int
	ContentType string
	Content     []byte
	Headers     Headers
}

// ErrorJSON returns a Response with an Error marshaled into bytes as content.
func ErrorJSON(statusCode int, err error, headers Headers) *Response {
	errResp := errorData{Error: err.Error()}

	b, err := json.Marshal(errResp)
	if err != nil {
		return ErrorResponse(http.StatusInternalServerError, err, headers)
	}

	return &Response{
		StatusCode:  statusCode,
		ContentType: contentTypeApplicationJSON,
		Content:     b,
		Headers:     headers,
	}
}

// ErrorResponse returns a Response with content of an error.
func ErrorResponse(statusCode int, err error, headers Headers) *Response {
	return &Response{
		StatusCode: statusCode,
		Content:    []byte(err.Error()),
		Headers:    headers,
	}
}

type errorData struct {
	Error string `json:"error"`
}

// JSON returns a Response with v marshaled into data as content.
func JSON(statusCode int, v interface{}, headers Headers) *Response {
	b, err := json.Marshal(data{v})
	if err != nil {
		return ErrorJSON(http.StatusInternalServerError, err, headers)
	}

	return &Response{
		StatusCode:  statusCode,
		ContentType: contentTypeApplicationJSON,
		Content:     b,
		Headers:     headers,
	}
}

type data struct {
	Data interface{} `json:"data"`
}
