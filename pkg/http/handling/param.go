package handling

import "context"

type urlParamHandler func(ctx context.Context, param string) string
