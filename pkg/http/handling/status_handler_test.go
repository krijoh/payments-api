package handling

import (
	"net/http"
	"payments-api/pkg/http/routing"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestNewStatusHandler(t *testing.T) {
	type args struct {
		hostname string
		version  string
	}

	tests := []struct {
		name string
		args args
	}{
		{
			"Initialize a new StatusHandler",
			args{"testhost", "0.1.0"},
		},
	}

	requires := require.New(t)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewStatusHandler(tt.args.hostname, tt.args.version)
			requires.NotNil(got)
		})
	}
}

// TODO: fix this test
func TestStatusHandler_GetStatus(t *testing.T) {
	type fields struct {
		Hostname string
		Version  string
	}
	type args struct {
		r *http.Request
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *routing.Response
	}{
		// TODO: Add test cases.
	}

	requires := require.New(t)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			sh := &StatusHandler{
				Hostname: tt.fields.Hostname,
				Version:  tt.fields.Version,
			}
			got := sh.GetStatus(tt.args.r)
			requires.NotNil(got)
		})
	}
}
