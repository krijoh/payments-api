package handling

import (
	"net/http"
	"payments-api/pkg/http/routing"
)

type StatusHandler struct {
	Hostname string `json:"hostname"`
	Version  string `json:"version"`
}

// NewStatusHandler initializes a StatusHandler.
func NewStatusHandler(hostname, version string) *StatusHandler {
	return &StatusHandler{Hostname: hostname, Version: version}
}

// GetStatus handles GET requests for status.
func (sh *StatusHandler) GetStatus(r *http.Request) *routing.Response {
	return routing.JSON(http.StatusOK, sh, nil)
}
