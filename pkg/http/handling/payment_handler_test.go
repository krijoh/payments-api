package handling

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"payments-api/pkg/errs"
	"payments-api/pkg/http/routing"
	"payments-api/pkg/logger"
	"payments-api/pkg/payment"

	"github.com/go-chi/chi"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/require"
)

func TestNewPaymentHandler(t *testing.T) {
	type args struct {
		log             logger.Logger
		svc             PaymentService
		urlParamHandler urlParamHandler
	}

	tests := []struct {
		name string
		args args
	}{
		{
			"Initalize a new PaymentHandler",
			args{
				log:             &logrus.Logger{},
				svc:             &MockPaymentService{},
				urlParamHandler: mockUrlParamHandler(""),
			},
		},
	}

	requires := require.New(t)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewPaymentHandler(tt.args.log, tt.args.svc, tt.args.urlParamHandler)
			requires.NotNil(got)
		})
	}
}

func TestPaymentHandler_GetAll(t *testing.T) {
	type fields struct {
		log logger.Logger
	}

	type args struct {
		url          string
		method       string
		mockArgs     []string
		mockReturns  []interface{}
		respPayments []*payment.Payment
	}

	tests := []struct {
		name   string
		fields fields
		args   args
		want   int
	}{
		{
			"GET all Payments and return 200",
			fields{
				log: &logrus.Logger{},
			},
			args{
				url:      "/payments/?page=1&limit=10",
				method:   "GET",
				mockArgs: []string{"1", "10"},
				mockReturns: []interface{}{
					[]*payment.Payment{
						&payment.Payment{Type: "1", Version: 0, OrganisationID: "1"},
						&payment.Payment{Type: "2", Version: 1, OrganisationID: "2"},
					},
					nil,
				},
				respPayments: []*payment.Payment{
					&payment.Payment{Type: "1", Version: 0, OrganisationID: "1"},
					&payment.Payment{Type: "2", Version: 1, OrganisationID: "2"}},
			},
			http.StatusOK,
		},
		{
			"GET all Payments without query parameters, should fail and return code 422",
			fields{
				log: &logrus.Logger{},
			},
			args{
				url:          "/payments/",
				method:       "GET",
				mockArgs:     []string{"1", "10"},
				mockReturns:  []interface{}{nil, nil},
				respPayments: nil,
			},
			http.StatusUnprocessableEntity,
		},
		{
			"GET all Payments when service returns error, should fail and return code 500",
			fields{
				log: &logrus.Logger{},
			},
			args{
				url:          "/payments/?page=1&limit=10",
				method:       "GET",
				mockArgs:     []string{"1", "10"},
				mockReturns:  []interface{}{nil, errors.New("query error")},
				respPayments: nil,
			},
			http.StatusInternalServerError,
		},
		{
			"GET all Payments with invalid query parameters, should fail and return code 422",
			fields{
				log: &logrus.Logger{},
			},
			args{
				url:          "/payments/?page=char&limit=1-",
				method:       "GET",
				mockArgs:     []string{"1", "asdf"},
				mockReturns:  []interface{}{nil, nil},
				respPayments: nil,
			},
			http.StatusUnprocessableEntity,
		},
	}

	requires := require.New(t)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mpr := &MockPaymentService{}
			ph := &PaymentHandler{
				log:             tt.fields.log,
				svc:             mpr,
				urlParamHandler: mockUrlParamHandler(""),
			}

			mpr.On("FindAll", tt.args.mockArgs[0], tt.args.mockArgs[1]).Return(tt.args.mockReturns...)

			recorder := httptest.NewRecorder()
			req := httptest.NewRequest("GET", tt.args.url, nil)

			h := http.HandlerFunc(routing.Route(ph.GetAll).ServeHTTP)
			h.ServeHTTP(recorder, req)

			requires.Equal(tt.want, recorder.Code)

			if recorder.Code >= http.StatusOK && recorder.Code < http.StatusMultipleChoices {
				mpr.AssertExpectations(t)
			}

			if tt.args.respPayments != nil {
				if len(tt.args.respPayments) > 1 {
					ps := struct {
						Data []payment.Payment `json:"data"`
					}{
						Data: []payment.Payment{},
					}
					if err := json.Unmarshal(recorder.Body.Bytes(), &ps); err != nil {
						t.Fatal("invalid body in response, no payments")
					}

					requires.Equal(len(tt.args.respPayments), len(ps.Data))
					return
				}
			}
		})
	}
}

func TestPaymentHandler_GetSingle(t *testing.T) {
	type fields struct {
		log logger.Logger
	}

	type args struct {
		url             string
		method          string
		mockArgs        string
		mockReturns     []interface{}
		respPayments    *payment.Payment
		urlParamHandler func(ctx context.Context, param string) string
	}

	tests := []struct {
		name   string
		fields fields
		args   args
		want   int
	}{
		{
			"GET a Payment",
			fields{
				log: &logrus.Logger{},
			},
			args{
				url:             "/payments/4ff1da70-7a27-11e9-8849-b70e5fd18dc",
				method:          "GET",
				mockArgs:        "4ff1da70-7a27-11e9-8849-b70e5fd18dc",
				mockReturns:     []interface{}{&payment.Payment{Type: "1", Version: 0, OrganisationID: "1", Attributes: &payment.Attributes{}}, nil},
				respPayments:    &payment.Payment{Type: "1", Version: 0, OrganisationID: "1", Attributes: &payment.Attributes{}},
				urlParamHandler: mockUrlParamHandler("4ff1da70-7a27-11e9-8849-b70e5fd18dc"),
			},
			http.StatusOK,
		},
		{
			"GET a Payment when service returns a database error, should fail and return code 500",
			fields{
				log: &logrus.Logger{},
			},
			args{
				url:             "/payments/4ff1da70-7a27-11e9-8849-b70e5fd18dc",
				method:          "GET",
				mockArgs:        "4ff1da70-7a27-11e9-8849-b70e5fd18dc",
				mockReturns:     []interface{}{nil, errs.DatabaseError{errors.New("query failed")}},
				respPayments:    nil,
				urlParamHandler: mockUrlParamHandler("4ff1da70-7a27-11e9-8849-b70e5fd18dc"),
			},
			http.StatusInternalServerError,
		},
		{
			"GET a Payment with invalid ID (UUID), should fail and return code 422",
			fields{
				log: &logrus.Logger{},
			},
			args{
				url:             "/payments/1",
				method:          "GET",
				mockArgs:        "1",
				mockReturns:     []interface{}{nil, errs.ValidationError{errors.New("invalid UUID")}, "invalid uuid"},
				respPayments:    nil,
				urlParamHandler: mockUrlParamHandler("1"),
			},
			http.StatusUnprocessableEntity,
		},
		{
			"GET a Payment with non existant ID, should fail and return code 404",
			fields{
				log: &logrus.Logger{},
			},
			args{
				url:             "/payments/023dce51-2afe-404b-924c-d4b00e9053e7",
				method:          "GET",
				mockArgs:        "023dce51-2afe-404b-924c-d4b00e9053e7",
				mockReturns:     []interface{}{nil, errors.New("payment not found")},
				respPayments:    nil,
				urlParamHandler: mockUrlParamHandler("023dce51-2afe-404b-924c-d4b00e9053e7"),
			},
			http.StatusNotFound,
		},
	}

	requires := require.New(t)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mpr := &MockPaymentService{}
			ph := &PaymentHandler{
				log:             tt.fields.log,
				svc:             mpr,
				urlParamHandler: tt.args.urlParamHandler,
			}

			mpr.On("FindByID", tt.args.mockArgs).Return(tt.args.mockReturns...)

			recorder := httptest.NewRecorder()
			req := httptest.NewRequest("GET", tt.args.url, nil)
			rctx := chi.NewRouteContext()
			rctx.URLParams.Add("id", "f716a852-7639-4559-9b0e-7437baf3d94a")

			h := http.HandlerFunc(routing.Route(ph.GetSingle).ServeHTTP)

			req = req.WithContext(context.WithValue(req.Context(), chi.RouteCtxKey, rctx))
			h.ServeHTTP(recorder, req)

			requires.Equal(tt.want, recorder.Code)

			if recorder.Code >= http.StatusOK && recorder.Code < http.StatusMultipleChoices {
				mpr.AssertExpectations(t)
			}

			if tt.args.respPayments != nil {
				p := struct {
					Data payment.Payment `json:"data"`
				}{
					Data: payment.Payment{},
				}

				if err := json.Unmarshal(recorder.Body.Bytes(), &p); err != nil {
					t.Fatal("invalid body in response, no payments")
				}
				require.True(t, len(p.Data.Type) > 0)
			}
		})
	}
}

func TestPaymentHandler_Post(t *testing.T) {
	type fields struct {
		log logger.Logger
	}

	type args struct {
		url          string
		method       string
		body         []byte
		mockArgs     *payment.Payment
		mockReturns  error
		respPayments bool
	}

	tests := []struct {
		name   string
		fields fields
		args   args
		want   int
	}{
		{
			"POST a Payment",
			fields{
				log: &logrus.Logger{},
			},
			args{
				url:          "/payments/",
				method:       "POST",
				body:         []byte(`{"type":"1","version":0,"organisation_id":"1","attributes":{}}`),
				mockArgs:     &payment.Payment{Type: "1", Version: 0, OrganisationID: "1", Attributes: &payment.Attributes{}},
				mockReturns:  nil,
				respPayments: true,
			},
			http.StatusCreated,
		},
		{
			"POST a Payment when service returns error, should fail and return code 500",
			fields{
				log: &logrus.Logger{},
			},
			args{
				url:          "/payments/",
				method:       "POST",
				body:         []byte(`{"type":"1","version":0,"organisation_id":"1","attributes":{}}`),
				mockArgs:     &payment.Payment{Type: "1", Version: 0, OrganisationID: "1", Attributes: &payment.Attributes{}},
				mockReturns:  errors.New("query failed"),
				respPayments: false,
			},
			http.StatusInternalServerError,
		},
		{
			"POST a Payment with invalid fields in JSON, should fail and return code 422",
			fields{
				log: &logrus.Logger{},
			},
			args{
				url:          "/payments/",
				method:       "POST",
				body:         []byte(`{"type":"1","version":0,"organisation_id":"1","attributes":{}}`),
				mockArgs:     &payment.Payment{Type: "1", Version: 0, OrganisationID: "1", Attributes: &payment.Attributes{}},
				mockReturns:  errs.ValidationError{errors.New("missing type field")},
				respPayments: false,
			},
			http.StatusUnprocessableEntity,
		},
	}

	requires := require.New(t)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mpr := &MockPaymentService{}
			ph := &PaymentHandler{
				log:             tt.fields.log,
				svc:             mpr,
				urlParamHandler: mockUrlParamHandler(""),
			}

			mpr.On("Create", tt.args.mockArgs).Return(tt.args.mockReturns)

			recorder := httptest.NewRecorder()
			req := httptest.NewRequest("POST", tt.args.url, bytes.NewBuffer(tt.args.body))

			h := http.HandlerFunc(routing.Route(ph.Post).ServeHTTP)
			h.ServeHTTP(recorder, req)

			requires.Equal(tt.want, recorder.Code)

			if recorder.Code >= http.StatusOK && recorder.Code < http.StatusMultipleChoices {
				mpr.AssertExpectations(t)
			}

			if tt.args.respPayments {
				ps := struct {
					Data *payment.Payment `json:"data"`
				}{
					Data: &payment.Payment{},
				}
				if err := json.Unmarshal(recorder.Body.Bytes(), &ps); err != nil {
					t.Fatal("invalid body in response, no payments")
				}

				requires.True(len(ps.Data.Type) > 0)
				requires.True(len(ps.Data.OrganisationID) > 0)
			}
		})
	}
}

func TestPaymentHandler_Delete(t *testing.T) {
	type fields struct {
		log logger.Logger
	}

	type args struct {
		url             string
		method          string
		mockArgs        string
		mockReturns     error
		urlParamHandler func(ctx context.Context, param string) string
	}

	tests := []struct {
		name   string
		fields fields
		args   args
		want   int
	}{
		{
			"DELETE a Payment",
			fields{
				log: &logrus.Logger{},
			},
			args{
				url:             "/payments/f716a852-7639-4559-9b0e-7437baf3d94a",
				method:          "DELETE",
				mockArgs:        "f716a852-7639-4559-9b0e-7437baf3d94a",
				mockReturns:     nil,
				urlParamHandler: mockUrlParamHandler("f716a852-7639-4559-9b0e-7437baf3d94a"),
			},
			http.StatusNoContent,
		},
		{
			"DELETE a Payment when service returns error, should fail and return 500",
			fields{
				log: &logrus.Logger{},
			},
			args{
				url:             "/payments/f716a852-7639-4559-9b0e-7437baf3d94a",
				method:          "DELETE",
				mockArgs:        "f716a852-7639-4559-9b0e-7437baf3d94a",
				mockReturns:     errs.DatabaseError{errors.New("query failed")},
				urlParamHandler: mockUrlParamHandler("f716a852-7639-4559-9b0e-7437baf3d94a"),
			},
			http.StatusInternalServerError,
		},
		{
			"DELETE a Payment when service cant find payment, should fail and return 404",
			fields{
				log: &logrus.Logger{},
			},
			args{
				url:             "/payments/f716a852-7639-4559-9b0e-7437baf3d94a",
				method:          "DELETE",
				mockArgs:        "f716a852-7639-4559-9b0e-7437baf3d94a",
				mockReturns:     errors.New("payment with id not found"),
				urlParamHandler: mockUrlParamHandler("f716a852-7639-4559-9b0e-7437baf3d94a"),
			},
			http.StatusNotFound,
		},
		{
			"DELETE a Payment when service returns validation error, should fail and return 422",
			fields{
				log: &logrus.Logger{},
			},
			args{
				url:             "/payments/f716a852-7639-4559-9b0e-7437baf3d94a",
				method:          "DELETE",
				mockArgs:        "f716a852-7639-4559-9b0e-7437baf3d94a",
				mockReturns:     errs.ValidationError{errors.New("invalid uuid used as id parameter")},
				urlParamHandler: mockUrlParamHandler("f716a852-7639-4559-9b0e-7437baf3d94a"),
			},
			http.StatusUnprocessableEntity,
		},
	}

	requires := require.New(t)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mpr := &MockPaymentService{}
			ph := &PaymentHandler{
				log:             tt.fields.log,
				svc:             mpr,
				urlParamHandler: tt.args.urlParamHandler,
			}

			mpr.On("DeleteByID", tt.args.mockArgs).Return(tt.args.mockReturns)

			recorder := httptest.NewRecorder()
			req := httptest.NewRequest("DELETE", tt.args.url, nil)
			rctx := chi.NewRouteContext()
			rctx.URLParams.Add("id", "f716a852-7639-4559-9b0e-7437baf3d94a")

			h := http.HandlerFunc(routing.Route(ph.Delete).ServeHTTP)

			req = req.WithContext(context.WithValue(req.Context(), chi.RouteCtxKey, rctx))
			h.ServeHTTP(recorder, req)

			requires.Equal(tt.want, recorder.Code)

			if recorder.Code >= http.StatusOK && recorder.Code < http.StatusMultipleChoices {
				mpr.AssertExpectations(t)
			}
		})
	}
}

func TestPaymentHandler_Put(t *testing.T) {
	type fields struct {
		log logger.Logger
	}

	type args struct {
		url             string
		method          string
		body            []byte
		mockArgs        *payment.Payment
		mockReturns     error
		respPayments    bool
		urlParamHandler func(ctx context.Context, param string) string
	}

	tests := []struct {
		name   string
		fields fields
		args   args
		want   int
	}{
		{
			"PUT a Payment",
			fields{
				log: &logrus.Logger{},
			},
			args{
				url:             "/payments/f716a852-7639-4559-9b0e-7437baf3d94a",
				method:          "PUT",
				body:            []byte(`{"type":"1","version":0,"organisation_id":"1"}`),
				mockArgs:        &payment.Payment{ID: "f716a852-7639-4559-9b0e-7437baf3d94a", Type: "1", Version: 0, OrganisationID: "1"},
				mockReturns:     nil,
				respPayments:    true,
				urlParamHandler: mockUrlParamHandler("f716a852-7639-4559-9b0e-7437baf3d94a"),
			},
			http.StatusAccepted,
		},
		{
			"PUT a Payment when service returns a DatabaseError, should fail and return code 500",
			fields{
				log: &logrus.Logger{},
			},
			args{
				url:             "/payments/f716a852-7639-4559-9b0e-7437baf3d94a",
				method:          "PUT",
				body:            []byte(`{"type":"1","version":0,"organisation_id":"1"}`),
				mockArgs:        &payment.Payment{ID: "f716a852-7639-4559-9b0e-7437baf3d94a", Type: "1", Version: 0, OrganisationID: "1"},
				mockReturns:     errs.DatabaseError{errors.New("query failed")},
				respPayments:    false,
				urlParamHandler: mockUrlParamHandler("f716a852-7639-4559-9b0e-7437baf3d94a"),
			},
			http.StatusInternalServerError,
		},
		{
			"PUT a Payment when service returns a ValidationError, should fail and return code 422",
			fields{
				log: &logrus.Logger{},
			},
			args{
				url:             "/payments/f716a852-7639-4559-9b0e-7437baf3d94a",
				method:          "PUT",
				body:            []byte(`{"type":"1","version":0,"organisation_id":"1"}`),
				mockArgs:        &payment.Payment{ID: "f716a852-7639-4559-9b0e-7437baf3d94a", Type: "1", Version: 0, OrganisationID: "1"},
				mockReturns:     errs.ValidationError{errors.New("validation of payment failed")},
				respPayments:    false,
				urlParamHandler: mockUrlParamHandler("f716a852-7639-4559-9b0e-7437baf3d94a"),
			},
			http.StatusUnprocessableEntity,
		},
		{
			"PUT a Payment with invalid JSON and service returns ValidationError, should fail and return code 422",
			fields{
				log: &logrus.Logger{},
			},
			args{
				url:             "/payments/f716a852-7639-4559-9b0e-7437baf3d94a",
				method:          "PUT",
				body:            []byte(`{"type,"version":0,"organisation_id":"1"}`),
				mockArgs:        &payment.Payment{ID: "f716a852-7639-4559-9b0e-7437baf3d94a", Type: "1", Version: 0, OrganisationID: "1"},
				mockReturns:     nil,
				respPayments:    false,
				urlParamHandler: mockUrlParamHandler("f716a852-7639-4559-9b0e-7437baf3d94a"),
			},
			http.StatusUnprocessableEntity,
		},
		{
			"PUT a Payment with non existant ID, should fail and return code 404",
			fields{
				log: &logrus.Logger{},
			},
			args{
				url:             "/payments/f716a852-7639-4559-9b0e-7437baf3d94a",
				method:          "PUT",
				body:            []byte(`{"type":"1","version":0,"organisation_id":"1"}`),
				mockArgs:        &payment.Payment{ID: "f716a852-7639-4559-9b0e-7437baf3d94a", Type: "1", Version: 0, OrganisationID: "1"},
				mockReturns:     errors.New("payment with id not found"),
				respPayments:    false,
				urlParamHandler: mockUrlParamHandler("f716a852-7639-4559-9b0e-7437baf3d94a"),
			},
			http.StatusNotFound,
		},
	}

	requires := require.New(t)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mpr := &MockPaymentService{}
			ph := &PaymentHandler{
				log:             tt.fields.log,
				svc:             mpr,
				urlParamHandler: tt.args.urlParamHandler,
			}

			mpr.On("Update", tt.args.mockArgs).Return(tt.args.mockReturns)

			recorder := httptest.NewRecorder()
			req := httptest.NewRequest("PUT", tt.args.url, bytes.NewBuffer(tt.args.body))

			h := http.HandlerFunc(routing.Route(ph.Put).ServeHTTP)
			h.ServeHTTP(recorder, req)

			requires.Equal(tt.want, recorder.Code)

			if recorder.Code >= http.StatusOK && recorder.Code < http.StatusMultipleChoices {
				mpr.AssertExpectations(t)
			}

			if tt.args.respPayments {
				ps := struct {
					Data *payment.Payment `json:"data"`
				}{
					Data: &payment.Payment{},
				}
				if err := json.Unmarshal(recorder.Body.Bytes(), &ps); err != nil {
					t.Fatal("invalid body in response, no payments")
				}

				requires.True(len(ps.Data.Type) > 0)
				requires.True(len(ps.Data.OrganisationID) > 0)
			}
		})
	}
}

func mockUrlParamHandler(id string) func(ctx context.Context, param string) string {
	return func(ctx context.Context, param string) string {
		return id
	}
}
