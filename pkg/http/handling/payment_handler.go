package handling

import (
	"encoding/json"
	"net/http"
	"strconv"

	"payments-api/pkg/errs"
	"payments-api/pkg/http/routing"
	"payments-api/pkg/logger"
	"payments-api/pkg/payment"

	"github.com/pkg/errors"
)

type PaymentService interface {
	FindByID(id string) (*payment.Payment, error)
	FindAll(page, limit string) ([]*payment.Payment, error)
	Create(p *payment.Payment) error
	DeleteByID(id string) error
	Update(p *payment.Payment) error
}

type PaymentHandler struct {
	log             logger.Logger
	svc             PaymentService
	urlParamHandler urlParamHandler
}

// NewPaymentHandler returns a http handler for payments
func NewPaymentHandler(log logger.Logger, svc PaymentService, urlParamHandler urlParamHandler) *PaymentHandler {
	return &PaymentHandler{log: log, svc: svc, urlParamHandler: urlParamHandler}
}

// GetAll handles GET requests for all Payments
func (ph *PaymentHandler) GetAll(r *http.Request) *routing.Response {
	page := r.URL.Query().Get("page")
	limit := r.URL.Query().Get("limit")
	if err := validatePageLimit(page, limit); err != nil {
		return routing.ErrorJSON(http.StatusUnprocessableEntity, err, nil)
	}

	payments, err := ph.svc.FindAll(page, limit)
	if err != nil {
		ph.log.Errorf("unable to find all payments, error: %v", err)
		return routing.ErrorJSON(http.StatusInternalServerError, errors.New("internal server error"), nil)
	}

	return routing.JSON(http.StatusOK, payments, nil)
}

func validatePageLimit(page, limit string) error {
	if len(page) < 1 || len(limit) < 1 {
		return errors.New("unable to parse required query parameters page and limit")
	}

	if _, err := strconv.Atoi(page); err != nil {
		return errors.New("invalid page parameter, not a number")
	}
	if _, err := strconv.Atoi(limit); err != nil {
		return errors.New("invalid limit parameter, not a number")
	}
	return nil
}

// GetSingle handles GET requests for a single Payment.
func (ph *PaymentHandler) GetSingle(r *http.Request) *routing.Response {
	ctx := r.Context()
	id := ph.urlParamHandler(ctx, "id")

	p, err := ph.svc.FindByID(id)
	if err != nil {
		switch errors.Cause(err).(type) {
		case errs.ValidationError:
			return routing.ErrorJSON(http.StatusUnprocessableEntity, err, nil)
		case errs.DatabaseError:
			ph.log.Errorf("unable to get a payment, error: %v", err)
			return routing.ErrorJSON(http.StatusInternalServerError, errors.New("internal server error"), nil)
		default:
			return routing.ErrorJSON(http.StatusNotFound, err, nil)
		}
	}

	return routing.JSON(http.StatusOK, p, nil)
}

// Post handles POST requests for a Payment.
func (ph *PaymentHandler) Post(r *http.Request) *routing.Response {
	var p payment.Payment

	if err := json.NewDecoder(r.Body).Decode(&p); err != nil {
		return routing.ErrorJSON(http.StatusUnprocessableEntity, errors.New("invalid JSON in request body"), nil)
	}

	p.ID = ""

	if err := ph.svc.Create(&p); err != nil {
		switch errors.Cause(err).(type) {
		case errs.ValidationError:
			return routing.ErrorJSON(http.StatusUnprocessableEntity, err, nil)
		default:
			ph.log.Errorf("unable to create payment %+v error: %v", p, err)
			return routing.ErrorJSON(http.StatusInternalServerError, errors.New("internal server error"), nil)
		}
	}

	return routing.JSON(http.StatusCreated, &p, nil)
}

// Delete handles DELETE requests and deletes a Payment
func (ph *PaymentHandler) Delete(r *http.Request) *routing.Response {
	ctx := r.Context()
	id := ph.urlParamHandler(ctx, "id")

	if err := ph.svc.DeleteByID(id); err != nil {
		switch errors.Cause(err).(type) {
		case errs.ValidationError:
			return routing.ErrorJSON(http.StatusUnprocessableEntity, err, nil)
		case errs.DatabaseError:
			ph.log.Errorf("unable to delete payment with ID %s, error: %v", id, err)
			return routing.ErrorJSON(
				http.StatusInternalServerError,
				errors.New("internal server error"),
				nil,
			)
		default:
			return routing.ErrorJSON(http.StatusNotFound, err, nil)
		}
	}

	return routing.JSON(http.StatusNoContent, nil, nil)
}

// Put handles PUT requests and updates a Payment
func (ph *PaymentHandler) Put(r *http.Request) *routing.Response {
	ctx := r.Context()
	id := ph.urlParamHandler(ctx, "id")

	var p payment.Payment
	p.ID = id

	if err := json.NewDecoder(r.Body).Decode(&p); err != nil {
		return routing.ErrorJSON(http.StatusUnprocessableEntity, errors.New("invalid JSON in request body"), nil)
	}

	if err := ph.svc.Update(&p); err != nil {
		switch errors.Cause(err).(type) {
		case errs.DatabaseError:
			ph.log.Errorf("unable to delete payment with ID %s, error: %v", id, err)
			return routing.ErrorJSON(
				http.StatusInternalServerError,
				errors.New("internal server error"),
				nil,
			)
		case errs.ValidationError:
			return routing.ErrorJSON(http.StatusUnprocessableEntity, err, nil)

		default:
			return routing.ErrorJSON(http.StatusNotFound, err, nil)
		}
	}

	return routing.JSON(http.StatusAccepted, &p, nil)
}
