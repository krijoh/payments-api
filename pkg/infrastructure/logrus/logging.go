// https://www.mountedthoughts.com/golang-logger-interface/
package logrus

import (
	"io"
	"io/ioutil"
	"os"
	"strings"

	"github.com/onrik/logrus/filename"
	loggr "github.com/sirupsen/logrus"
)

type UTCFormatter struct {
	loggr.Formatter
}

// Format will format the log message with UTC time
func (u UTCFormatter) Format(e *loggr.Entry) ([]byte, error) {
	e.Time = e.Time.UTC()
	return u.Formatter.Format(e)
}

// Setup logger
func Setup(logFormat, logLevel, logFile string) (*loggr.Logger, error) {
	logger := loggr.New()

	logger.AddHook(filename.NewHook())

	setFormat(logFormat, logger)
	setOutput(logFile, logger)
	setLevel(logLevel, logger)

	return logger, nil
}

func setFormat(logFormat string, logger *loggr.Logger) {
	switch strings.ToLower(logFormat) {
	case "text":
		logger.SetFormatter(&UTCFormatter{&loggr.TextFormatter{TimestampFormat: "2006-01-02T15:04:05.000", FullTimestamp: true}})
	case "json":
		logger.SetFormatter(&UTCFormatter{&loggr.JSONFormatter{TimestampFormat: "2006-01-02T15:04:05.000"}})
	default:
		logger.Warnf("unknown logformat '%s', falling back to 'text' format.", logFormat)
		logger.SetFormatter(&UTCFormatter{&loggr.TextFormatter{}})
	}
}

func setOutput(logFile string, logger *loggr.Logger) {
	switch strings.ToLower(logFile) {
	case "stdout":
		logger.SetOutput(os.Stdout)
	case "null":
		logger.SetOutput(ioutil.Discard)
	default:
		if _, err := os.Stat(logFile); os.IsNotExist(err) {
			_, err = os.Create(logFile)
			if err != nil {
				logger.Warnf("unable to open logfile '%s', falling back to stdout: %v", logFile, err)
				logger.SetOutput(os.Stdout)
				return
			}
		}

		file, err := os.OpenFile(logFile, os.O_APPEND|os.O_WRONLY, 0640)
		if err != nil {
			logger.Warnf("unable to open logfile '%s', falling back to stdout: %v", logFile, err)
			logger.SetOutput(os.Stdout)
			return
		}
		multiWriter := io.MultiWriter(os.Stdout, file)
		logger.SetOutput(multiWriter)
	}
}

func setLevel(logLevel string, logger *loggr.Logger) {
	switch strings.ToLower(logLevel) {
	case "debug":
		logger.Info("setting debug level")
		logger.SetLevel(loggr.DebugLevel)
	case "info":
		logger.Info("setting info level")
		logger.SetLevel(loggr.InfoLevel)
	case "warning":
		logger.Info("setting warning level")
		logger.SetLevel(loggr.WarnLevel)
	case "error":
		logger.Info("setting error level")
		logger.SetLevel(loggr.ErrorLevel)
	default:
		logger.Warnf("unknown loglevel '%s', falling back to 'info' level.", logLevel)
		logger.SetLevel(loggr.InfoLevel)
	}
}
