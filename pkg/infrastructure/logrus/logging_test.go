package logrus

import (
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestInit(t *testing.T) {
	type args struct {
		logFormat string
		logLevel  string
		logFile   string
	}
	tests := []struct {
		name string
		args args
	}{
		{
			"JSON, debug loglevel, stdout output",
			args{"json", "debug", "stdout"},
		},
		{
			"JSON, warning loglevel, file and stdout output",
			args{"json", "warning", "testdata/logtest.log"},
		},
		{
			"Text, info loglevel, discard output",
			args{"text", "info", "null"},
		},
		{
			"Unset logformat, error loglevel, unset output",
			args{logLevel: "error"},
		},
		{
			"Unset logformat, unset loglevel, unset output",
			args{},
		},
	}

	asserts := assert.New(t)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := Setup(tt.args.logFormat, tt.args.logLevel, tt.args.logFile)
			asserts.Nil(err)

			if _, err := os.Stat(tt.args.logFile); err == nil {
				if err = os.Remove(tt.args.logFile); err != nil {
					fmt.Println("failed to remove temporary logfile")
				}
			}
		})
	}
}
