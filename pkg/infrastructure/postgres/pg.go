package postgres

import (
	"fmt"
	"log"
	"time"

	"github.com/go-pg/pg"
)

// Config for database.
type Config struct {
	Host string
	User string
	Pass string
	Port int
	Name string
}

// Connect to the database.
// Close it after using it.
func Connect(config Config) *pg.DB {
	addr := fmt.Sprintf("%s:%d", config.Host, config.Port)
	db := pg.Connect(&pg.Options{
		Addr:         addr,
		User:         config.User,
		Password:     config.Pass,
		Database:     config.Name,
		ReadTimeout:  time.Second * 6,
		WriteTimeout: time.Second * 6,
	})

	if _, err := db.Exec("SELECT 1"); err != nil {
		log.Fatalf("unable to connect to database using address %s, %v", addr, err)
	}

	return db
}
