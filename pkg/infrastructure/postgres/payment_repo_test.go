package postgres

import (
	"payments-api/pkg/payment"
	"testing"

	"github.com/go-pg/pg"
	"github.com/stretchr/testify/require"
)

func TestNewPaymentRepo(t *testing.T) {
	type args struct {
		db *pg.DB
	}

	tests := []struct {
		name string
		args args
	}{
		{
			"Initalize a new PaymentRepo",
			args{Connect(Config{Host: pgHost, Port: pgPort, User: pgUser, Pass: pgPass, Name: pgDb})},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewPaymentRepo(tt.args.db)
			require.NotNil(t, got)
		})
	}
}

func TestPaymentRepo_FindPaymentByID(t *testing.T) {
	type fields struct {
		db *pg.DB
	}

	type args struct {
		id string
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *payment.Payment
		wantErr bool
	}{
		{
			"Find a payment by ID",
			fields{Connect(Config{Host: pgHost, Port: pgPort, User: pgUser, Pass: pgPass, Name: pgDb})},
			args{"2ebf1bba-7a27-11e9-8849-17607cdd54af"},
			&payment.Payment{ID: "2ebf1bba-7a27-11e9-8849-17607cdd54af"},
			false,
		},
		{
			"Find a payment by ID with non-existant ID, should fail",
			fields{Connect(Config{Host: pgHost, Port: pgPort, User: pgUser, Pass: pgPass, Name: pgDb})},
			args{"2ebf1bba-7a27-11e9-8849-17607cdd54ab"},
			nil,
			true,
		},
	}

	requires := require.New(t)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			pr := PaymentRepo{db: tt.fields.db}

			got, err := pr.FindPaymentByID(tt.args.id)
			if tt.wantErr {
				requires.Error(err)
				return
			}
			requires.NoError(err)
			requires.NotNil(got)
		})
	}
}

func TestPaymentRepo_FindAllPayments(t *testing.T) {
	type fields struct {
		db *pg.DB
	}

	type args struct {
		page  string
		limit string
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		{
			"Find all payments",
			fields{Connect(Config{Host: pgHost, Port: pgPort, User: pgUser, Pass: pgPass, Name: pgDb})},
			args{"1", "10"},
			2,
			false,
		},
		{
			"Find all payments with invalid page and limit, should fail",
			fields{Connect(Config{Host: pgHost, Port: pgPort, User: pgUser, Pass: pgPass, Name: pgDb})},
			args{"invalid", "invald"},
			0,
			true,
		},
		{
			"Find all payments should return an empty slice",
			fields{Connect(Config{Host: pgHost, Port: pgPort, User: pgUser, Pass: pgPass, Name: pgDb})},
			args{"invalid", "invald"},
			0,
			false,
		},
	}

	requires := require.New(t)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			pr := PaymentRepo{db: tt.fields.db}

			got, err := pr.FindAllPayments(tt.args.page, tt.args.limit)
			if tt.wantErr {
				requires.Error(err)
				return
			}
			if !tt.wantErr && tt.want == 0 {
				return
			}
			requires.NoError(err)
			requires.Equal(tt.want, len(got))
		})
	}
}

func TestPaymentRepo_CreatePayment(t *testing.T) {
	type fields struct {
		db *pg.DB
	}

	type args struct {
		p *payment.Payment
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *payment.Payment
		wantErr bool
	}{
		{
			"Create a new payment",
			fields{Connect(Config{Host: pgHost, Port: pgPort, User: pgUser, Pass: pgPass, Name: pgDb})},
			args{
				&payment.Payment{
					ID:             "4ff1da70-7a27-11e9-8849-b70e5fd18dca",
					Type:           "Payment",
					Version:        0,
					OrganisationID: "123",
				},
			},
			&payment.Payment{
				ID:             "4ff1da70-7a27-11e9-8849-b70e5fd18dca",
				Type:           "Payment",
				Version:        0,
				OrganisationID: "123",
				Attributes:     &payment.Attributes{},
			},
			false,
		},
		{
			"Create a new payment with duplicate ID, should fail",
			fields{Connect(Config{Host: pgHost, Port: pgPort, User: pgUser, Pass: pgPass, Name: pgDb})},
			args{
				&payment.Payment{
					ID:             "4ff1da70-7a27-11e9-8849-b70e5fd18d11",
					Type:           "Payment",
					Version:        2,
					OrganisationID: "123",
				},
			},
			nil,
			true,
		},
	}

	requires := require.New(t)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			pr := PaymentRepo{db: tt.fields.db}

			err := pr.CreatePayment(tt.args.p)
			if tt.wantErr {
				requires.Error(err)
				return
			}
			requires.NoError(err)
			requires.Equal(tt.want.ID, tt.args.p.ID)
			requires.Equal(tt.want.Type, tt.args.p.Type)
			requires.Equal(tt.want.Version, tt.args.p.Version)
			requires.Equal(tt.want.OrganisationID, tt.args.p.OrganisationID)
			requires.Equal(tt.want.Attributes, tt.args.p.Attributes)
		})
	}
}

func TestPaymentRepo_DeletePaymentByID(t *testing.T) {
	type fields struct {
		db *pg.DB
	}

	type args struct {
		id string
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			"Delete a payment",
			fields{Connect(Config{Host: pgHost, Port: pgPort, User: pgUser, Pass: pgPass, Name: pgDb})},
			args{"4ff1da70-7a27-11e9-8849-b70e5fd18d11"},
			false,
		},
		{
			"Delete a payment with non-existand ID, should fail",
			fields{Connect(Config{Host: pgHost, Port: pgPort, User: pgUser, Pass: pgPass, Name: pgDb})},
			args{"4ff1da70-7a27-11e9-8849-b70e5fd18hug"},
			true,
		},
	}

	requires := require.New(t)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			pr := PaymentRepo{db: tt.fields.db}

			err := pr.DeletePaymentByID(tt.args.id)
			if tt.wantErr {
				requires.Error(err)
				return
			}
			requires.NoError(err)
		})
	}
}

func TestPaymentRepo_UpdatePayment(t *testing.T) {
	type fields struct {
		db *pg.DB
	}

	type args struct {
		p *payment.Payment
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *payment.Payment
		wantErr bool
	}{
		{
			"Update a payment",
			fields{Connect(Config{Host: pgHost, Port: pgPort, User: pgUser, Pass: pgPass, Name: pgDb})},
			args{
				&payment.Payment{
					ID:             "2ebf1bba-7a27-11e9-8849-17607cdd54af",
					Type:           "PaymentModified",
					Version:        2,
					OrganisationID: "123",
					Attributes:     &payment.Attributes{},
				},
			},
			&payment.Payment{
				ID:             "2ebf1bba-7a27-11e9-8849-17607cdd54af",
				Type:           "PaymentModified",
				Version:        2,
				OrganisationID: "123",
				Attributes:     &payment.Attributes{},
			},
			false,
		},
		{
			"Update a payment with invalid ID, should fail",
			fields{Connect(Config{Host: pgHost, Port: pgPort, User: pgUser, Pass: pgPass, Name: pgDb})},
			args{
				&payment.Payment{
					ID:             "bebf1bba-7a27-11e9-8849-17607cdd54af",
					Type:           "PaymentModified",
					Version:        2,
					OrganisationID: "123",
					Attributes:     &payment.Attributes{},
				},
			},
			nil,
			true,
		},
	}

	requires := require.New(t)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			pr := PaymentRepo{db: tt.fields.db}

			err := pr.UpdatePayment(tt.args.p)
			if tt.wantErr {
				requires.Error(err)
				return
			}
			requires.NoError(err)
			requires.Equal(tt.want, tt.args.p)
		})
	}
}
