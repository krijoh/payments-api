package postgres

import (
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"testing"

	"github.com/go-pg/pg"
	migrate "github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/ory/dockertest"
	"github.com/pkg/errors"
)

const (
	pgHost = "localhost"
	pgUser = "postgres"
	pgPass = "postgres"
	pgDb   = "payments"
)

var pgPort int

func TestMain(m *testing.M) {
	resource, err := startPostgresContainer()
	if err != nil {
		log.Fatalf("unable to start postgres container, %v", err)
	}

	exitCode := m.Run()

	if err := stopPostgresContainer(resource); err != nil {
		log.Fatalf("unable to stop postgres container, %v", err)
	}

	os.Exit(exitCode)
}

func TestConnect(t *testing.T) {
	type args struct {
		config Config
	}

	tests := []struct {
		name string
		args args
	}{
		{
			"Connect to postgres",
			args{Config{Host: pgHost, Port: pgPort, User: pgUser, Pass: pgUser, Name: pgDb}},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			Connect(tt.args.config)
		})
	}
}

func startPostgresContainer() (*dockertest.Resource, error) {
	pool, err := dockertest.NewPool("")
	if err != nil {
		return nil, errors.Wrap(err, "unable to create docker pool")
	}

	resource, err := pool.RunWithOptions(
		&dockertest.RunOptions{
			Repository: "postgres",
			Tag:        "latest",
			Env:        []string{"POSTGRES_PASSWORD=" + pgPass, "POSTGRES_USER=" + pgUser, "POSTGRES_DB=" + pgDb},
		})
	if err != nil {
		return nil, errors.Wrap(err, "unable to run postgres container")
	}

	if err = resource.Expire(180); err != nil {
		return nil, errors.Wrap(err, "unable to set expire on postgres container")
	}

	pgPort, err = strconv.Atoi(resource.GetPort("5432/tcp"))
	if err != nil {
		return nil, errors.Wrap(err, "unable to convert postgres port from string to int")
	}

	if err = pool.Retry(func() error {
		pgConfig := pg.Options{
			Addr:     fmt.Sprintf("%s:%d", pgHost, pgPort),
			User:     pgUser,
			Password: pgPass,
			Database: pgDb,
		}

		db := pg.Connect(&pgConfig)
		_, err = db.Exec("SELECT 1")
		return err
	}); err != nil {
		return nil, errors.Wrap(err, "postgres connection unhealthy")
	}

	err = migrateDatabase("/testdata", "up")
	if err != nil {
		return nil, errors.Wrap(err, "unable to migrate database up")
	}
	return resource, nil
}

func stopPostgresContainer(resource io.Closer) error {
	if err := migrateDatabase("/testdata", "down"); err != nil {
		return errors.Wrap(err, "unable to migrate database down")
	}

	if err := resource.Close(); err != nil {
		return errors.Wrap(err, "unable to close postgres container")
	}

	return nil
}

func migrateDatabase(path, action string) error {
	workingPath, err := os.Getwd()
	if err != nil {
		return errors.Wrap(err, "unable to get current working directory")
	}

	m, err := migrate.New(
		fmt.Sprintf("file://%s", filepath.Join(workingPath, path)),
		fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable", pgUser, pgPass, pgHost, pgPort, pgDb),
	)
	if err != nil {
		return errors.Wrap(err, "unable to init migration instance")
	}

	if action == "up" {
		if err = m.Up(); err != nil {
			return errors.Wrap(err, "unable to run up migrations")
		}
	}

	if action == "down" {
		if err = m.Down(); err != nil {
			return errors.Wrap(err, "unable to run down migrations")
		}
	}

	return nil
}
