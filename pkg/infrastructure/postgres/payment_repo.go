package postgres

import (
	"payments-api/pkg/errs"
	"payments-api/pkg/payment"

	"github.com/go-pg/pg"
	"github.com/go-pg/pg/urlvalues"
	"github.com/pkg/errors"
)

type PaymentRepo struct {
	db *pg.DB
}

// NewPaymentRepo initializes a PaymentRepo using parameter db.
func NewPaymentRepo(db *pg.DB) *PaymentRepo {
	return &PaymentRepo{db}
}

// FindPaymentByID uses id parameter to find a payment.
func (pr PaymentRepo) FindPaymentByID(id string) (*payment.Payment, error) {
	p := &payment.Payment{ID: id}
	err := pr.db.Select(p)
	switch {
	case err == pg.ErrNoRows:
		return nil, errors.Errorf("no payment found with ID %s", id)
	case err != nil:
		return nil, errors.Wrapf(errs.DatabaseError{err}, "unable to retrieve payment with ID %s", p.ID)
	}

	return p, nil
}

// FindAllPayments retrieves all payments using pagination. If no Payment's is found an empty slice is returned.
func (pr PaymentRepo) FindAllPayments(page, limit string) ([]*payment.Payment, error) {
	paginationValues := urlvalues.Values{"page": []string{page}, "limit": []string{limit}}

	p := []*payment.Payment{}
	err := pr.db.Model(&p).Order("created_at ASC").Apply(urlvalues.Pagination(paginationValues)).Select()
	switch {
	case err == pg.ErrNoRows:
		return p, nil
	case err != nil:
		return nil, errors.Errorf("unable to retrieve payments using pagination query page=%s?limit=%s", page, limit)
	}

	return p, nil
}

// CreatePayment inserts a new Payment. The struct Payment used as parameter is mutated.
func (pr PaymentRepo) CreatePayment(p *payment.Payment) error {
	if err := pr.db.Insert(p); err != nil {
		return errors.Wrapf(errs.DatabaseError{err}, "unable to create payment %+v", p)
	}

	return nil
}

// DeletePaymentByID deletes a payment by id.
func (pr PaymentRepo) DeletePaymentByID(id string) error {
	p := payment.Payment{ID: id}
	err := pr.db.Delete(&p)
	switch {
	case err == pg.ErrNoRows:
		return errors.Errorf("no payment found with ID %s", id)
	case err != nil:
		return errors.Wrapf(errs.DatabaseError{err}, "unable to delete payment with ID %s", p.ID)
	}

	return nil
}

// UpdatePayment updates a payment. The struct Payment used as parameter is mutated.
func (pr PaymentRepo) UpdatePayment(p *payment.Payment) error {
	err := pr.db.Update(p)
	switch {
	case err == pg.ErrNoRows:
		return errors.Errorf("no payment found with ID %s", p.ID)
	case err != nil:
		return errors.Wrapf(errs.DatabaseError{err}, "unable to update payment with ID: %s", p.ID)
	}

	return nil
}
