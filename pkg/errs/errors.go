package errs

import "fmt"

type DatabaseError struct {
	Err error
}

func (pe DatabaseError) Error() string {
	return fmt.Sprintf("postgres returned an error: %v", pe.Err)
}

type ValidationError struct {
	Err error
}

func (ve ValidationError) Error() string {
	return fmt.Sprintf("validation error: %v", ve.Err)
}
