package config

import (
	"github.com/caarlos0/env"
	"github.com/pkg/errors"
)

// Config contains configurations for the application.
type Config struct {
	Version  string `env:"VERSION,required"`
	Hostname string `env:"HOSTNAME,required"`
	Port     int    `env:"PORT" envDefault:"8000"`
	Database
	Logger
}

type Database struct {
	Host string `env:"DATABASE_HOST,required"`
	Port int    `env:"DATABASE_PORT,required"`
	User string `env:"DATABASE_USER,required"`
	Pass string `env:"DATABASE_PASS,required"`
	Name string `env:"DATABASE_NAME,required"`
}

type Logger struct {
	Level  string `env:"LOG_LEVEL" envDefault:"info"`
	Format string `env:"LOG_FORMAT" envDefault:"json"`
	File   string `env:"LOG_FILE" envDefault:"stdout"`
}

// Load config from environment variables.
func Load() (*Config, error) {
	var c Config
	if err := env.Parse(&c); err != nil {
		return nil, errors.Wrap(err, "unable to parse config")
	}
	if err := env.Parse(&c.Database); err != nil {
		return nil, errors.Wrap(err, "unable to parse database config")
	}
	if err := env.Parse(&c.Logger); err != nil {
		return nil, errors.Wrap(err, "unable to parse logger config")
	}

	return &c, nil
}
