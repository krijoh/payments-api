package config

import (
	"os"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestLoad(t *testing.T) {
	tests := []struct {
		name    string
		want    *Config
		wantErr bool
	}{
		{
			name: "Set Config using environment variables",
			want: &Config{
				Version:  "0.0.1",
				Hostname: "testhost",
				Port:     8000,
				Database: Database{
					Host: "localhost",
					Port: 5432,
					User: "postgres",
					Pass: "postgres",
					Name: "paymentsapi",
				},
				Logger: Logger{
					Level:  "info",
					Format: "json",
					File:   "stdout",
				},
			},
			wantErr: false,
		},
	}

	requires := require.New(t)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var wg sync.WaitGroup

			setEnv(&wg)

			got, err := Load()
			if tt.wantErr {
				requires.Error(err)
				return
			}

			requires.NoError(err)
			requires.Equal(tt.want, got)

			wg.Wait()
		})
	}
}

func setEnv(wg *sync.WaitGroup) {
	envSlice := map[string]string{
		"VERSION":       "0.0.1",
		"HOSTNAME":      "testhost",
		"DATABASE_HOST": "localhost",
		"DATABASE_PORT": "5432",
		"DATABASE_USER": "postgres",
		"DATABASE_PASS": "postgres",
		"DATABASE_NAME": "paymentsapi",
		"LOG_LEVEL":     "info",
		"LOG_FORMAT":    "json",
		"LOG_FILE":      "stdout",
	}

	for key, value := range envSlice {
		os.Setenv(key, value)
	}

	wg.Add(1)
	go func() {
		time.Sleep(time.Millisecond * 500)
		for _, key := range envSlice {
			os.Unsetenv(key)
		}
		wg.Done()
	}()
}
