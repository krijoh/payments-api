package main

import (
	"payments-api/pkg/http/handling"
	"testing"

	"github.com/stretchr/testify/require"
)

func Test_newPaymentServer(t *testing.T) {
	type args struct {
		handler *handling.PaymentHandler
	}

	tests := []struct {
		name string
		args args
	}{
		{
			"Initialize a new payment server",
			args{&handling.PaymentHandler{}},
		},
	}

	requires := require.New(t)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := newPaymentServer(tt.args.handler)
			requires.NotNil(got)
		})
	}
}

func Test_paymentServer_endpoints(t *testing.T) {
	type fields struct {
		handler *handling.PaymentHandler
	}
	tests := []struct {
		name   string
		fields fields
	}{
		{
			"Create endpoints on router for payments",
			fields{&handling.PaymentHandler{}},
		},
	}

	requires := require.New(t)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := &paymentServer{
				handler: tt.fields.handler,
			}
			got := h.endpoints()
			requires.True(len(got.Routes()) == 2)
		})
	}
}
