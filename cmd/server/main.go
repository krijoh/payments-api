package main

import (
	"log"

	"payments-api/pkg/config"
	"payments-api/pkg/infrastructure/logrus"
)

func main() {
	cfg, err := config.Load()
	if err != nil {
		log.Println("unable to set configuration, exiting")
		log.Fatal(err)
	}

	logger, err := logrus.Setup(cfg.Format, cfg.Level, cfg.File)
	if err != nil {
		log.Println("unable to setup logger, exiting")
		log.Fatal(err)
	}

	s := newServer(logger, cfg)
	errChan := s.run()
	logger.Fatalf("server terminated by %v", <-errChan)
}
