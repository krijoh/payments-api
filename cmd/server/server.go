package main

import (
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"payments-api/pkg/config"
	"payments-api/pkg/http/handling"
	"payments-api/pkg/http/routing"
	"payments-api/pkg/infrastructure/postgres"
	"payments-api/pkg/logger"
	"payments-api/pkg/payment"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

type server struct {
	log logger.Logger
	cfg *config.Config
}

// newServer initializes a Server.
func newServer(log logger.Logger, cfg *config.Config) *server {
	return &server{
		log: log,
		cfg: cfg,
	}
}

// run will setup dependencies and start server.
func (s *server) run() chan error {
	pgConfig := postgres.Config{
		Host: s.cfg.Database.Host,
		Port: s.cfg.Database.Port,
		User: s.cfg.Database.User,
		Pass: s.cfg.Database.Pass,
		Name: s.cfg.Database.Name,
	}
	db := postgres.Connect(pgConfig)

	paymentRepo := postgres.NewPaymentRepo(db)
	paymentSvc := payment.New(paymentRepo)

	r := chi.NewRouter()

	r.Use(
		middleware.RequestID,
		middleware.RealIP,
		middleware.RequestLogger(&middleware.DefaultLogFormatter{
			Logger:  s.log,
			NoColor: true,
		}),
		middleware.Timeout(10*time.Second),
	)

	statusHandler := handling.NewStatusHandler(s.cfg.Hostname, s.cfg.Version)
	paymentHandler := handling.NewPaymentHandler(s.log, paymentSvc, chi.URLParamFromCtx)

	v1 := chi.NewRouter()
	r.Get("/status", routing.HandlerFunc(statusHandler.GetStatus))
	v1.Route("/payments", func(r chi.Router) {
		ps := newPaymentServer(paymentHandler)
		r.Mount("/", ps.endpoints())
	})

	r.Mount("/api/v1", v1)

	errChan := make(chan error, 2)
	go func() {
		s.log.Infof("server running on port %d", s.cfg.Port)
		errChan <- http.ListenAndServe(fmt.Sprintf(":%d", s.cfg.Port), r)
	}()

	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errChan <- fmt.Errorf("%s", <-c)
	}()

	return errChan
}
