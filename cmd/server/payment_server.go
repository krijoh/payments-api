package main

import (
	"payments-api/pkg/http/handling"
	"payments-api/pkg/http/routing"

	"github.com/go-chi/chi"
)

type paymentServer struct {
	handler *handling.PaymentHandler
}

func newPaymentServer(handler *handling.PaymentHandler) *paymentServer {
	return &paymentServer{handler}
}

func (ps *paymentServer) endpoints() chi.Router {
	r := chi.NewRouter()
	r.Get("/", routing.HandlerFunc(ps.handler.GetAll))
	r.Get("/{id}", routing.HandlerFunc(ps.handler.GetSingle))
	r.Post("/", routing.HandlerFunc(ps.handler.Post))
	r.Put("/{id}", routing.HandlerFunc(ps.handler.Put))
	r.Delete("/{id}", routing.HandlerFunc(ps.handler.Delete))
	return r
}
