package main

import (
	"fmt"
	"io"
	"log"
	"os"
	"payments-api/pkg/config"
	"payments-api/pkg/logger"
	"strconv"
	"testing"
	"time"

	"github.com/go-pg/pg"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/ory/dockertest"
	"github.com/pkg/errors"
	loggr "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/require"
)

const (
	pgHost = "localhost"
	pgUser = "postgres"
	pgPass = "postgres"
	pgDb   = "paymentsapi"
)

var pgPort int

func TestMain(m *testing.M) {
	resource, err := startPostgresContainer()
	if err != nil {
		log.Fatalf("unable to start postgres container, %v", err)
	}

	exitCode := m.Run()

	if err := stopPostgresContainer(resource); err != nil {
		log.Fatalf("unable to stop postgres container, %v", err)
	}

	os.Exit(exitCode)
}

func TestNewServer(t *testing.T) {
	type args struct {
		log logger.Logger
		cfg *config.Config
	}

	tests := []struct {
		name string
		args args
	}{
		{
			"Initialize a new Server",
			args{&loggr.Logger{}, &config.Config{}},
		},
	}

	requires := require.New(t)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := newServer(tt.args.log, tt.args.cfg)
			requires.NotNil(got)
		})
	}
}

func TestServer_Run(t *testing.T) {
	type fields struct {
		log logger.Logger
		cfg *config.Config
	}

	tests := []struct {
		name   string
		fields fields
	}{
		{
			"Initialize a new Server",
			fields{
				&loggr.Logger{},
				&config.Config{Database: config.Database{Host: pgHost, Port: pgPort, User: pgUser, Pass: pgUser, Name: pgDb}}},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &server{
				log: tt.fields.log,
				cfg: tt.fields.cfg,
			}

			errChan := s.run()

			time.Sleep(time.Second * 1)
			errChan <- fmt.Errorf("test quit")

		})
	}
}

func startPostgresContainer() (*dockertest.Resource, error) {
	pool, err := dockertest.NewPool("")
	if err != nil {
		return nil, errors.Wrap(err, "unable to create docker pool")
	}

	resource, err := pool.RunWithOptions(
		&dockertest.RunOptions{
			Repository: "postgres",
			Tag:        "latest",
			Env:        []string{"POSTGRES_PASSWORD=" + pgPass, "POSTGRES_USER=" + pgUser, "POSTGRES_DB=" + pgDb},
		})
	if err != nil {
		return nil, errors.Wrap(err, "unable to run postgres container")
	}

	if err = resource.Expire(180); err != nil {
		return nil, errors.Wrap(err, "unable to set expire on postgres container")
	}

	pgPort, err = strconv.Atoi(resource.GetPort("5432/tcp"))
	if err != nil {
		return nil, errors.Wrap(err, "unable to convert postgres port from string to int")
	}

	if err = pool.Retry(func() error {
		pgConfig := pg.Options{
			Addr:     fmt.Sprintf("%s:%d", pgHost, pgPort),
			User:     pgUser,
			Password: pgPass,
			Database: pgDb,
		}

		db := pg.Connect(&pgConfig)
		_, err = db.Exec("SELECT 1")
		return err
	}); err != nil {
		return nil, errors.Wrap(err, "postgres connection unhealthy")
	}

	return resource, nil
}

func stopPostgresContainer(resource io.Closer) error {
	return resource.Close()
}
