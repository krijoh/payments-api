# Usage
The API provides the following endpoints for managing payments.

## Get a single payment
  Returns a single payment in JSON by using the id parameter.

* **URL**

  /api/v1/payments/:id

* **Method:**

  `GET`

* **Success Response:**
  * **Code:** 200
  * **Info:** Returns a payment
  * **Example JSON response:**
    ```json
    {
      "data": {
        "type": "Payment",
        "id": "4ee3a8d8-ca7b-4290-a52c-dd5b6165ec43",
        "version": 0,
        "organisation_id": "743d5b63-8e6f-432e-a8fa-c5d8d2ee5fcb",
        "attributes": {
          "amount": "100.21",
          "beneficiary_party": {
            "account_name": "W Owens",
            "account_number": "31926819",
            "account_number_code": "BBAN",
            "account_type": 0,
            "address": "1 The Beneficiary Localtown SE2",
            "bank_id": "403000",
            "bank_id_code": "GBDSC",
            "name": "Wilfred Jeremiah Owens"
          },
          "charges_information": {
            "bearer_code": "SHAR",
            "sender_charges": [
              {
                "amount": "5.00",
                "currency": "GBP"
              },
              {
                "amount": "10.00",
                "currency": "USD"
              }
            ],
            "receiver_charges_amount": "1.00",
            "receiver_charges_currency": "USD"
          },
          "currency": "GBP",
          "debtor_party": {
            "account_name": "EJ Brown Black",
            "account_number": "GB29XABC10161234567801",
            "account_number_code": "IBAN",
            "address": "10 Debtor Crescent Sourcetown NE1",
            "bank_id": "203301",
            "bank_id_code": "GBDSC",
            "name": "Emelia Jane Brown"
          },
          "end_to_end_reference": "Wil piano Jan",
          "fx": {
            "contract_reference": "FX123",
            "exchange_rate": "2.00000",
            "original_amount": "200.42",
            "original_currency": "USD"
          },
          "numeric_reference": "1002001",
          "payment_id": "123456789012345678",
          "payment_purpose": "Paying for goods/services",
          "payment_scheme": "FPS",
          "payment_type": "Credit",
          "processing_date": "2017-01-18",
          "reference": "Payment for Em's piano lessons",
          "scheme_payment_sub_type": "InternetBanking",
          "scheme_payment_type": "ImmediatePayment",
          "sponsor_party": {
            "account_number": "56781234",
            "bank_id": "123123",
            "bank_id_code": "GBDSC"
          }
        }
      }
    }
    ```

* **Error Response:**
  * **Code:** 404
  * **Info:** Payment not found

  OR

  * **Code:** 422
  * **Info:** Invalid ID , must be an UUID

  OR

  * **Code:** 500
  * **Info:** Internal server error

## Get all payments
  Returns a collection of payments in JSON by using the query parameters page and limit.

* **URL**

  /api/v1/payments/?page=1&limit=10

* **Method:**

  `GET`

* **Success Response:**
  * **Code:** 200
  * **Info:** Returns payments
  * **Example JSON response:**
    ```json
    {
      "data": [
            {
              "type": "Payment",
              "id": "4ee3a8d8-ca7b-4290-a52c-dd5b6165ec43",
              "version": 0,
              "organisation_id": "743d5b63-8e6f-432e-a8fa-c5d8d2ee5fcb",
              "attributes": {
                "amount": "100.21",
                "beneficiary_party": {
                  "account_name": "W Owens",
                  "account_number": "31926819",
                  "account_number_code": "BBAN",
                  "account_type": 0,
                  "address": "1 The Beneficiary Localtown SE2",
                  "bank_id": "403000",
                  "bank_id_code": "GBDSC",
                  "name": "Wilfred Jeremiah Owens"
                },
                "charges_information": {
                  "bearer_code": "SHAR",
                  "sender_charges": [
                    {
                      "amount": "5.00",
                      "currency": "GBP"
                    },
                    {
                      "amount": "10.00",
                      "currency": "USD"
                    }
                  ],
                  "receiver_charges_amount": "1.00",
                  "receiver_charges_currency": "USD"
                },
                "currency": "GBP",
                "debtor_party": {
                  "account_name": "EJ Brown Black",
                  "account_number": "GB29XABC10161234567801",
                  "account_number_code": "IBAN",
                  "address": "10 Debtor Crescent Sourcetown NE1",
                  "bank_id": "203301",
                  "bank_id_code": "GBDSC",
                  "name": "Emelia Jane Brown"
                },
                "end_to_end_reference": "Wil piano Jan",
                "fx": {
                  "contract_reference": "FX123",
                  "exchange_rate": "2.00000",
                  "original_amount": "200.42",
                  "original_currency": "USD"
                },
                "numeric_reference": "1002001",
                "payment_id": "123456789012345678",
                "payment_purpose": "Paying for goods/services",
                "payment_scheme": "FPS",
                "payment_type": "Credit",
                "processing_date": "2017-01-18",
                "reference": "Payment for Em's piano lessons",
                "scheme_payment_sub_type": "InternetBanking",
                "scheme_payment_type": "ImmediatePayment",
                "sponsor_party": {
                  "account_number": "56781234",
                  "bank_id": "123123",
                  "bank_id_code": "GBDSC"
                }
              }
            }
        ]
    }
    ```

* **Error Response:**
  * **Code:** 422
  * **Info:** Invalid query parameters.

  OR

  * **Code:** 500
  * **Info:** Internal server error

## Create a payment
  Creates a payment using the JSON in the request body.

* **URL**

  /api/v1/payments/

* **Method:**

  `POST`

* **Request:**
  * **Example JSON body in request:**
    ```json
    {
      "type": "Payment",
      "version": 0,
      "organisation_id": "743d5b63-8e6f-432e-a8fa-c5d8d2ee5fcb",
      "attributes": {
        "amount": "100.21",
        "beneficiary_party": {
          "account_name": "W Owens",
          "account_number": "31926819",
          "account_number_code": "BBAN",
          "account_type": 0,
          "address": "1 The Beneficiary Localtown SE2",
          "bank_id": "403000",
          "bank_id_code": "GBDSC",
          "name": "Wilfred Jeremiah Owens"
        },
        "charges_information": {
          "bearer_code": "SHAR",
          "sender_charges": [
            {
              "amount": "5.00",
              "currency": "GBP"
            },
            {
              "amount": "10.00",
              "currency": "USD"
            }
          ],
          "receiver_charges_amount": "1.00",
          "receiver_charges_currency": "USD"
        },
        "currency": "GBP",
        "debtor_party": {
          "account_name": "EJ Brown Black",
          "account_number": "GB29XABC10161234567801",
          "account_number_code": "IBAN",
          "address": "10 Debtor Crescent Sourcetown NE1",
          "bank_id": "203301",
          "bank_id_code": "GBDSC",
          "name": "Emelia Jane Brown"
        },
        "end_to_end_reference": "Wil piano Jan",
        "fx": {
          "contract_reference": "FX123",
          "exchange_rate": "2.00000",
          "original_amount": "200.42",
          "original_currency": "USD"
        },
        "numeric_reference": "1002001",
        "payment_id": "123456789012345678",
        "payment_purpose": "Paying for goods/services",
        "payment_scheme": "FPS",
        "payment_type": "Credit",
        "processing_date": "2017-01-18",
        "reference": "Payment for Em's piano lessons",
        "scheme_payment_sub_type": "InternetBanking",
        "scheme_payment_type": "ImmediatePayment",
        "sponsor_party": {
          "account_number": "56781234",
          "bank_id": "123123",
          "bank_id_code": "GBDSC"
        }
      }
    }
    ```

* **Success Response:**
  * **Code:** 201
  * **Info:** Returns created payment
  * **Example JSON response:**
    ```json
    {
      "data": {
        "type": "Payment",
        "id": "4ee3a8d8-ca7b-4290-a52c-dd5b6165ec43",
        "version": 0,
        "organisation_id": "743d5b63-8e6f-432e-a8fa-c5d8d2ee5fcb",
        "attributes": {
          "amount": "100.21",
          "beneficiary_party": {
            "account_name": "W Owens",
            "account_number": "31926819",
            "account_number_code": "BBAN",
            "account_type": 0,
            "address": "1 The Beneficiary Localtown SE2",
            "bank_id": "403000",
            "bank_id_code": "GBDSC",
            "name": "Wilfred Jeremiah Owens"
          },
          "charges_information": {
            "bearer_code": "SHAR",
            "sender_charges": [
              {
                "amount": "5.00",
                "currency": "GBP"
              },
              {
                "amount": "10.00",
                "currency": "USD"
              }
            ],
            "receiver_charges_amount": "1.00",
            "receiver_charges_currency": "USD"
          },
          "currency": "GBP",
          "debtor_party": {
            "account_name": "EJ Brown Black",
            "account_number": "GB29XABC10161234567801",
            "account_number_code": "IBAN",
            "address": "10 Debtor Crescent Sourcetown NE1",
            "bank_id": "203301",
            "bank_id_code": "GBDSC",
            "name": "Emelia Jane Brown"
          },
          "end_to_end_reference": "Wil piano Jan",
          "fx": {
            "contract_reference": "FX123",
            "exchange_rate": "2.00000",
            "original_amount": "200.42",
            "original_currency": "USD"
          },
          "numeric_reference": "1002001",
          "payment_id": "123456789012345678",
          "payment_purpose": "Paying for goods/services",
          "payment_scheme": "FPS",
          "payment_type": "Credit",
          "processing_date": "2017-01-18",
          "reference": "Payment for Em's piano lessons",
          "scheme_payment_sub_type": "InternetBanking",
          "scheme_payment_type": "ImmediatePayment",
          "sponsor_party": {
            "account_number": "56781234",
            "bank_id": "123123",
            "bank_id_code": "GBDSC"
          }
        }
        }
      }
    }
    ```

* **Error Response:**
  * **Code:** 422
  * **Info:** Invalid JSON in body

  OR

  * **Code:** 500
  * **Info:** Internal server error


## Update a payment
  Updates a payment using the parameter and the JSON in the request body.

* **URL**

  /api/v1/payments/:id

* **Method:**

  `PUT`

* **Request:**
  * **Example JSON body in request:**
    ```json
    {
      "type": "Payment",
      "version": 0,
      "organisation_id": "743d5b63-8e6f-432e-a8fa-c5d8d2ee5fcb",
      "attributes": {
        "amount": "100.21",
        "beneficiary_party": {
          "account_name": "W Owens",
          "account_number": "31926819",
          "account_number_code": "BBAN",
          "account_type": 0,
          "address": "1 The Beneficiary Localtown SE2",
          "bank_id": "403000",
          "bank_id_code": "GBDSC",
          "name": "Wilfred Jeremiah Owens"
        },
        "charges_information": {
          "bearer_code": "SHAR",
          "sender_charges": [
            {
              "amount": "5.00",
              "currency": "GBP"
            },
            {
              "amount": "10.00",
              "currency": "USD"
            }
          ],
          "receiver_charges_amount": "1.00",
          "receiver_charges_currency": "USD"
        },
        "currency": "GBP",
        "debtor_party": {
          "account_name": "EJ Brown Black",
          "account_number": "GB29XABC10161234567801",
          "account_number_code": "IBAN",
          "address": "10 Debtor Crescent Sourcetown NE1",
          "bank_id": "203301",
          "bank_id_code": "GBDSC",
          "name": "Emelia Jane Brown"
        },
        "end_to_end_reference": "Wil piano Jan",
        "fx": {
          "contract_reference": "FX123",
          "exchange_rate": "2.00000",
          "original_amount": "200.42",
          "original_currency": "USD"
        },
        "numeric_reference": "1002001",
        "payment_id": "123456789012345678",
        "payment_purpose": "Paying for goods/services",
        "payment_scheme": "FPS",
        "payment_type": "Credit",
        "processing_date": "2017-01-18",
        "reference": "Payment for Em's piano lessons",
        "scheme_payment_sub_type": "InternetBanking",
        "scheme_payment_type": "ImmediatePayment",
        "sponsor_party": {
          "account_number": "56781234",
          "bank_id": "123123",
          "bank_id_code": "GBDSC"
        }
      }
    }
    ```

* **Success Response:**
  * **Code:** 202
  * **Info:** Returns created payment
  * **Example JSON response:**
    ```json
    {
      "data": {
        "type": "Payment",
        "id": "4ee3a8d8-ca7b-4290-a52c-dd5b6165ec43",
        "version": 0,
        "organisation_id": "743d5b63-8e6f-432e-a8fa-c5d8d2ee5fcb",
        "attributes": {
          "amount": "100.21",
          "beneficiary_party": {
            "account_name": "W Owens",
            "account_number": "31926819",
            "account_number_code": "BBAN",
            "account_type": 0,
            "address": "1 The Beneficiary Localtown SE2",
            "bank_id": "403000",
            "bank_id_code": "GBDSC",
            "name": "Wilfred Jeremiah Owens"
          },
          "charges_information": {
            "bearer_code": "SHAR",
            "sender_charges": [
              {
                "amount": "5.00",
                "currency": "GBP"
              },
              {
                "amount": "10.00",
                "currency": "USD"
              }
            ],
            "receiver_charges_amount": "1.00",
            "receiver_charges_currency": "USD"
          },
          "currency": "GBP",
          "debtor_party": {
            "account_name": "EJ Brown Black",
            "account_number": "GB29XABC10161234567801",
            "account_number_code": "IBAN",
            "address": "10 Debtor Crescent Sourcetown NE1",
            "bank_id": "203301",
            "bank_id_code": "GBDSC",
            "name": "Emelia Jane Brown"
          },
          "end_to_end_reference": "Wil piano Jan",
          "fx": {
            "contract_reference": "FX123",
            "exchange_rate": "2.00000",
            "original_amount": "200.42",
            "original_currency": "USD"
          },
          "numeric_reference": "1002001",
          "payment_id": "123456789012345678",
          "payment_purpose": "Paying for goods/services",
          "payment_scheme": "FPS",
          "payment_type": "Credit",
          "processing_date": "2017-01-18",
          "reference": "Payment for Em's piano lessons",
          "scheme_payment_sub_type": "InternetBanking",
          "scheme_payment_type": "ImmediatePayment",
          "sponsor_party": {
            "account_number": "56781234",
            "bank_id": "123123",
            "bank_id_code": "GBDSC"
          }
        }
      }
    }
    ```

* **Error Response:**
  * **Code:** 404
  * **Info:** Payment with ID from parameter not found

  OR

  * **Code:** 422
  * **Info:** Invalid JSON in body or invalid id parameter

  OR

  * **Code:** 500
  * **Info:** Internal server error

## Delete a payment
  Deletes a payment using the id parameter

* **URL**

  /api/v1/payments/:id

* **Method:**

  `DELETE`

* **Success Response:**
  * **Code:** 204
  * **Info:** Payment deleted

* **Error Response:**
  * **Code:** 404
  * **Info:** Payment with id not found

  OR

  * **Code:** 422
  * **Info:** Invalid id parameter.

  OR

  * **Code:** 500
  * **Info:** Internal server error


