[![pipeline status](https://gitlab.com/krijoh/payments-api/badges/master/pipeline.svg)](https://gitlab.com/krijoh/payments-api/commits/master) [![coverage report](https://gitlab.com/krijoh/payments-api/badges/master/coverage.svg)](https://gitlab.com/krijoh/payments-api/commits/master)

# Payments API
The Payments API is a REST service which stores information about a payment in a database. It stores information such as for example amount, beneficiary and debtor for a payment.

The application is built with Go and uses Postgres as a storage.
Example manifests for a deployment to Kubernetes is included. In the manifests a Postgres database is deployed to Kubernetes which the API uses as storage. For production use it's recommended to run the database outside Kubernetes, so the Postgres deployment is only included to get the application up and running for testing purposes. During migration the database is seeded with a couple of payments so one can query the API after deployment.

The application is intended to run in Kubernetes so it has no authorization enabled since a common setup with Kubernetes is to use an API gateway which handles authorization plus other things as for example TLS termination and rate limiting.

A goal during development of the application was to deliver clean and loosely coupled code. Dependencies are passed in as arguments, utilizing dependency injection.
Therefor interfaces is used for example the database repository, so one easily can swap out the storage from Postgres to for example MongoDB.
The following describes how the parts in the application are connected and passed.

Database->Repository-->Service-->Handler-->Server

API usage documentation can be found [here](https://gitlab.com/krijoh/payments-api/blob/master/USAGE.md)

### Installation

The API can be installed on Kubernetes using the manifests located in `deploymens/kubernetes`.
Docker images for the API and migrations for Postgres is available on Docker hub which the manifests uses.
Before deploying the host value of the ingress manifest must be changed.

Deploy on Kubernetes by running the following command.

```sh
$ kubectl apply -f deployments/kubernetes/paymentsapi.yaml
```

### Testing

To test the application it is required to have Docker installed, then run the following.
```sh
$ make test
```
